//
//  Tarea.swift
//  Proyecto
//
//  Created by Macosx on 6/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import Foundation

class Tarea: NSObject, NSCoding {
    
    // MARK: Variables
    var Descripcion: String //Descripcion de la tarea a realiar
    var Dia: Date
    
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("tareas")
    
    
    //MARK types
    struct PropertyKey {
        static let Descripcion = "Descripcion"
        static let Dia = "Dia"
    }
    
    public init (Descripcion: String, Dia: Date) {
        self.Descripcion = Descripcion
        self.Dia = Dia
    }
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(Descripcion, forKey: PropertyKey.Descripcion)
        aCoder.encode(Dia, forKey: PropertyKey.Dia)
        
    }
    required convenience init(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        let Descripcion=aDecoder.decodeObject(forKey: PropertyKey.Descripcion) as? String
        
        let Dia = aDecoder.decodeObject(forKey: PropertyKey.Dia) as? Date
        
        self.init(Descripcion: Descripcion!, Dia: Dia!)
        
    }
    
}
