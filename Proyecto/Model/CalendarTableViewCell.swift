//
//  CalendarTableViewCell.swift
//  Proyecto
//
//  Created by Macosx on 18/5/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {
    
    // MARK: Atributos
    @IBOutlet weak var inicio: UILabel!
    @IBOutlet weak var fin: UILabel!
    @IBOutlet weak var nombre: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
