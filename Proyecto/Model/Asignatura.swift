//
//  Asignatura.swift
//  Proyecto
//
//  Created by Jose on 26/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import Foundation
import JTAppleCalendar
import os.log

class Asignatura: NSObject, NSCoding{
    
    
    //MARK: Propiedades
    var nombreAsignatura: String
    var horarioInicio: [Int : Date]
    var horarioFin: [Int : Date]
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("asignaturas")
    
    //MARK: Types
    struct PropertyKey {
        static let nombreAsignatura = "nombreAsignatura"
        static let horarioInicio = "horarioInicio"
        static let horarioFin = "horarioFin"
    }
    
    public init(nombreAsignatura: String, horarioInicio: [Int:Date], horarioFin: [Int:Date]){
        self.nombreAsignatura = nombreAsignatura
        self.horarioInicio = horarioInicio
        self.horarioFin = horarioFin
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(nombreAsignatura, forKey: PropertyKey.nombreAsignatura)
        aCoder.encode(horarioInicio, forKey: PropertyKey.horarioInicio)
        aCoder.encode(horarioFin, forKey: PropertyKey.horarioFin)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        let nombreAsignatura=aDecoder.decodeObject(forKey: PropertyKey.nombreAsignatura) as? String
        
        let horarioInicio = aDecoder.decodeObject(forKey: PropertyKey.horarioInicio) as? [Int:Date]
        let horarioFin = aDecoder.decodeObject(forKey: PropertyKey.horarioFin) as? [Int:Date]
        
        self.init(nombreAsignatura: nombreAsignatura!, horarioInicio: horarioInicio!, horarioFin: horarioFin!)
    }
    
}
