//
//  PeriodoLectivo.swift
//  Proyecto
//
//  Created by Jose on 26/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import Foundation

class PeriodoLectivo: NSObject, NSCoding{
    var fechaInicio: Date
    var fechaFin: Date
    
    
    
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("periodolectivo")
    
    //MARK: Keys
    struct PropertyKey {
        static let fechaInicio = "fechaInicio"
        static let fechaFin = "fechaFin"
    }
    
    // MARK: Initializer
    public init(fechaInicio: Date, fechaFin: Date){
        self.fechaInicio	= fechaInicio
        self.fechaFin = fechaFin
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fechaInicio, forKey: PropertyKey.fechaInicio)
        aCoder.encode(fechaFin, forKey: PropertyKey.fechaFin)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        let fechaInicio=aDecoder.decodeObject(forKey: PropertyKey.fechaInicio) as? Date
        
        let fechaFin = aDecoder.decodeObject(forKey: PropertyKey.fechaFin) as? Date
        
        self.init(fechaInicio: fechaInicio!, fechaFin: fechaFin!)
        
    }
}

