//
//  TareaTableViewCell.swift
//  Proyecto
//
//  Created by Macosx on 6/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit

class TareaTableViewCell: UITableViewCell {

    // MARK: Propiedades
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var horario: UILabel!
    @IBOutlet weak var descripcion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
