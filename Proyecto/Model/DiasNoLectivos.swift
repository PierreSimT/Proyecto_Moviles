//
//  DiasNoLectivos.swift
//  Proyecto
//
//  Created by Jose on 26/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import Foundation
import os.log
class DiasNoLectivos: NSObject, NSCoding{
    
    // MARK Propiedades
    var nombre: String
    var dianol: Date
    
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("diasnolectivos")
    
    
    //MARK types
    struct PropertyKey {
        static let nombre = "nombre"
        static let dianol = "dianol"
    }
    
    // MARK: Initialization
    public init(nombre: String, dianol: Date){
        self.nombre = nombre
        self.dianol = dianol
    }
    
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(nombre, forKey: PropertyKey.nombre)
        aCoder.encode(dianol, forKey: PropertyKey.dianol)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        let nombre=aDecoder.decodeObject(forKey: PropertyKey.nombre) as? String
        
        let dianol = aDecoder.decodeObject(forKey: PropertyKey.dianol) as? Date
        
        self.init(nombre: nombre!, dianol: dianol!)    }
}
