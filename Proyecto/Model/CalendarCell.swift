//
//  CalendarCell.swift
//  Proyecto
//
//  Created by Macosx on 13/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {

    // MARK: Atributos
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    var diaSemana: DaysOfWeek!
    var fecha: Date!
    
    let diccionario: [Int : DaysOfWeek] = [ 1: .monday, 2: .tuesday, 3: .wednesday, 4: .thursday, 5: .friday]
    
    let formatter = DateFormatter()
    
    /**
    Obtenemos las asignaturas asociadas a esta fecha
     - returns: vector de Any con las asignaturas que se realizan en este dia
    */
    func getDatos() -> [Any?] {

        let asignaturas = NSKeyedUnarchiver.unarchiveObject(withFile: Asignatura.ArchiveURL.path) as? [Asignatura]
        
        var resultados: [NSObject] = []
        
        formatter.dateFormat = "dddd"
        
        if let asigs = asignaturas {
            for asignatura in asigs {
                for ( dia, _ ) in asignatura.horarioInicio {
                    let diaAsignatura = diccionario[dia]
                    if ( diaSemana == diaAsignatura ) {
                        resultados.append(asignatura)
                    }
                }
            }
        }
        
        return resultados
        
    }
}
