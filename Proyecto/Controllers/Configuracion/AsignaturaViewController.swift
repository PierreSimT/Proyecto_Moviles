//
//  AsignaturaViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 14/5/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit

class AsignaturaViewController: UIViewController {

    // MARK: Atributos
    var asignaturas = [Asignatura]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    // MARK: - Navigation
    /**
        Prepara los datos dependiendo del segue, en caso que se crea una nueva asignatura, entonces no se hace nada
        en caso que se quiera modificar una asignatura existente, se deben de pasar los datos de la asignatura correspondiente.
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is ConfiguracionTableViewController {
            
        } else {
            switch(segue.identifier ?? "") {
            case "addAsignatura":
                print("Nueva asignatura")
            case "editAsignatura":
                guard let nuevaAsignaturaControlador = segue.destination as? NuevaAsignaturaViewController else {
                    fatalError("Unknown destination")
                }
                
                guard let celdaAsignatura = sender as? UITableViewCell else {
                    fatalError("Unknown sender")
                }
                
                guard let indexPath = tableView.indexPath(for: celdaAsignatura) else {
                    fatalError("Reminder not in tableView")
                }
                
                let asignaturaSeleccionada = asignaturas[indexPath.row]
                nuevaAsignaturaControlador.asignatura = asignaturaSeleccionada
                
            default:
                fatalError("Segue no reconocido")
            }
        }
        
    }
    
    /**
        Unwind que actualiza los datos, en caso de modificar una asignatura, entonces la actualiza,
        en caso de crear una nueva, se añade a la estructura de datos
    */
    @IBAction func unwindToAsignatura(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? NuevaAsignaturaViewController, let asignatura = sourceViewController.asignatura {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                
                asignaturas[selectedIndexPath.row] = asignatura
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
                
            } else {
                let newIndexPath = IndexPath(row: asignaturas.count, section: 0)
                
                asignaturas.append(asignatura)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
        
    }
    
}

// MARK: Funciones para tabla
extension AsignaturaViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return asignaturas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "celdaAsignatura")
        
        let asignatura = asignaturas[indexPath.row]
        
        // Obtenemos los atributos
        let nombre = asignatura.nombreAsignatura
        
        cell.textLabel?.text = nombre
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        asignaturas.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        
    }
    
    // MARK: Accion de boton editar
    
    @IBAction func editarTabla (_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
    
    
}
