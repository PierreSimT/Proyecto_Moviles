//
//  NuevaTareaViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 15/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import Eureka

class NuevaTareaViewController: FormViewController {
    
    var tareas: [Tarea]?
    var valoresDatos: [ String:  Any?] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        loadForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Funciones de formulario
    /**
        Carga el formulario mediante el framework de Eureka
     */
    func loadForm(){
        form +++
            MultivaluedSection(multivaluedOptions: [.Reorder, .Insert, .Delete],
                               header: "Añadir Tareas") {
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Añadir Nueva Tarea"
                                    }
                                }
                                if let valores = tareas {
                                    for tarea in valores {
                                        let row = SplitRow<TextRow,DateTimeRow>() { celda in
                                            celda.rowLeft = TextRow() {
                                                $0.value = tarea.Descripcion
                                            }
                                            celda.rowRight = DateTimeRow() {
                                                $0.value = tarea.Dia
                                            }
                                            celda.rowLeftPercentage = CGFloat(0.5)
                                            }
                                        $0.append(row)
                                    }
                                }
                                $0.multivaluedRowToInsertAt = { index in
                                    return SplitRow<TextRow, DateTimeRow>() {
                                        $0.rowLeft = TextRow() {
                                            $0.placeholder = "Nombre Tarea"
                                        }
                                        $0.rowRight = DateTimeRow() {
                                            $0.value = Date()
                                        }
                                    }
                                }
        }
    }

    // MARK: Acciones
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        } else {
            fatalError("Not inside navigation controller.")
        }
    }
    
}
