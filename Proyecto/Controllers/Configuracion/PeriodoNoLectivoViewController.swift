//
//  PeriodoNoLectivoViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 15/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import Eureka

class PeriodoNoLectivoViewController: FormViewController {

    // MARK: Atributos
    var datos: [PeriodoLectivo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Funciones de formulario
    /**
        Carga el formulario mediante el framework de Eureka
    */
    func loadForm(){
        form +++ Section("Comienzo Periodo Lectivo")
            <<< DateRow("inicio") {
                $0.title = "Fecha Comienzo"
                if let valores = datos, datos?.count == 1 {
                    $0.value = valores[0].fechaInicio
                } else {
                    $0.value = Date(timeIntervalSinceReferenceDate: 0)
                }
            }
            +++ Section("Fin de Periodo Lectivo")
            <<< DateRow("fin") {
                $0.title = "Fecha de Finalización"
                if let valores = datos, datos?.count == 1 {
                    $0.value = valores[0].fechaFin
                } else {
                    $0.value = Date(timeIntervalSinceReferenceDate: 0)
                }
            }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        } else {
            fatalError("Not inside navigation controller.")
        }
        
    }
    
}
