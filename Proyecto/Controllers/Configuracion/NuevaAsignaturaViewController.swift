//
//  NuevaAsignaturaViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 15/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Eureka

class NuevaAsignaturaViewController: FormViewController {

    var asignatura: Asignatura?
    var secciones: [Section] = []
    let diccionarioString: [String: Int] = [ "Lunes": 1, "Martes": 2, "Miercoles": 3, "Jueves": 4, "Viernes": 5 ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Funciones de formulario
    /**
        Carga el formulario mediante el framework de Eureka, en caso que se modifique una asignatura
        ya existente, se muestran los datos
     */
    func loadForm(){

        form +++ Section("Informacion necesaria")
            <<< TextRow(){ row in
                    row.title = "Nombre"
                    row.placeholder = "Asignatura"
                    if let nombre = asignatura?.nombreAsignatura {
                        row.value = nombre
                    }
                }
            
            <<< MultipleSelectorRow<String>("asignatura") {
                    $0.title = "Dias de la semana"
                    $0.selectorTitle = "Dias de la semana"
                    $0.options = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"]
                    if let valores = asignatura?.horarioInicio {
                        var seleccionados: Set<String> = []
                        for ( dia, _ ) in valores {
                            seleccionados.insert($0.options![dia-1])
                        }
                        $0.value = seleccionados
                    }
                
                }
        cargarSecciones()
    }
    
    /**
        Funcion llamada desde el loadForm(), crea las secciones que son unicas para cada
        dia de la semana y las esconde hasta que hayan sido seleccionadas en el MultipleSelectorRow
    */
    func cargarSecciones () {
        
        let valores = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"]
        for indice in 0...4 {
            form +++ Section("\(valores[indice])") {
                $0.hidden = Condition.function(["asignatura"], {form in
                    let celda = form.rowBy(tag: "asignatura") as? MultipleSelectorRow<String>
                    let resultado = celda?.value
                    let esta = resultado?.contains(valores[indice]) ?? false
                    return !esta
                })
                }
                <<< TimeRow() {
                    $0.title = "Comienzo"
                    if let valorInicio = asignatura?.horarioInicio {
                        $0.value = valorInicio[indice+1]
                    } else {
                        $0.value = Date()
                    }
                }
                <<< TimeRow() {
                    $0.title = "Final"
                    if let valorFin = asignatura?.horarioFin {
                        $0.value = valorFin[indice+1]
                    } else {
                        $0.value = Date()
                    }
                }
        }
        
    }
    
    func loadAsignaturas () -> [Asignatura]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Asignatura.ArchiveURL.path) as? [Asignatura]
    }
    
    
    // MARK: - Navigation

    /**
        Antes de realizar el unwind, se guardan los datos de la asignatura que se quiere crear o modificar
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let todoCeldas = form.rows
        var nombre: String = ""
        var horarioInicio: [Int: Date] = [:]
        var horarioFin: [Int: Date] = [:]
        
        for celda in todoCeldas {
            
            if let celdaNombre = celda as? TextRow {
                nombre = celdaNombre.value!
            }
            
            if let celdaHorario = celda as? TimeRow {
                
                if celdaHorario.title == "Comienzo" {
                    let dia = celdaHorario.section?.header?.title
                    horarioInicio[diccionarioString[dia!]!] = celdaHorario.value
                } else {
                    let dia = celdaHorario.section?.header?.title
                    horarioFin[diccionarioString[dia!]!] = celdaHorario.value
                }
                
            }
            
        }
        
        asignatura = Asignatura(nombreAsignatura: nombre, horarioInicio: horarioInicio, horarioFin: horarioFin)
        
    }
    

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        let isAdding = presentingViewController is UINavigationController
        
        if isAdding {
            dismiss(animated: true, completion: nil)
        } else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        } else {
            fatalError("Not inside navigation controller.")
        }
    }

}
