//
//  ConfiguracionTableViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 15/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import Eureka

class ConfiguracionTableViewController: UITableViewController {

    var tareas = [Tarea]()
    var asignaturas = [Asignatura]()
    var dNoLectivos = [DiasNoLectivos]()
    var pLectivos = [PeriodoLectivo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        cargarDatos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    // MARK: Carga datos almacenados
    /**
        Carga de datos almacenados localmente
    */
    func cargarDatos() {
        if let tar = NSKeyedUnarchiver.unarchiveObject(withFile: Tarea.ArchiveURL.path) as? [Tarea] {
            tareas = tar
        }
        if let asig = NSKeyedUnarchiver.unarchiveObject(withFile: Asignatura.ArchiveURL.path) as? [Asignatura] {
            asignaturas = asig
        }
        if let dLectivos = NSKeyedUnarchiver.unarchiveObject(withFile: DiasNoLectivos.ArchiveURL.path) as? [DiasNoLectivos] {
            dNoLectivos = dLectivos
        }
        if let pLect = NSKeyedUnarchiver.unarchiveObject(withFile: PeriodoLectivo.ArchiveURL.path) as? [PeriodoLectivo] {
            pLectivos = pLect
        }
    }
    
    // MARK: - Navigation

    /**
        Se prepara la informacion que se va a enviar a cada uno de los segues conectados con esta vista.
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.destination is HomeViewController {
            
        } else {
            switch(segue.identifier ?? "") {
                
            case "periodoLectivo":
                guard let periodoLectivoControlador = segue.destination as?  PeriodoNoLectivoViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                periodoLectivoControlador.datos = pLectivos
                
                
            case "diasNoLectivos":
                guard let diasNoLectivosControlador = segue.destination as? DiasNoLectivosViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                diasNoLectivosControlador.dias = dNoLectivos
                
            case "anadirAsignaturas":
                guard let nuevAsignaturaControlador = segue.destination as? AsignaturaViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                nuevAsignaturaControlador.asignaturas = asignaturas
                
            case "anadirTareas":
                guard let nuevTareaControlador = segue.destination as? NuevaTareaViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                nuevTareaControlador.tareas = tareas
            
            default:
                fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            }
        }
    }
    
    /**
        Accion unwind que realizan todas las vistas de configuracion conectadas con esta vista.
        Se actualizan los datos actuales con los cambios que se han realizado
    */
    @IBAction func unwindToConfiguracion(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? NuevaTareaViewController {
            
            let valores = sourceViewController.form.allRows
            var auxiliar = [Tarea]()
            
            for elemento in valores {
                
                if let celda = elemento as? SplitRow<TextRow, DateTimeRow> {
                    
                    if let desc = celda.rowLeft?.value, let dia = celda.rowRight?.value {
                        
                        let nueva = Tarea(Descripcion: desc, Dia: dia)
                        auxiliar.append(nueva)
                    }
                    
                }
                
            }
            
            tareas = auxiliar
            
            NSKeyedArchiver.archiveRootObject(tareas, toFile: Tarea.ArchiveURL.path)
            
        }
        if let sourceViewController = sender.source as? AsignaturaViewController {
            
            let valores = sourceViewController.asignaturas
            
            asignaturas = valores
            
            NSKeyedArchiver.archiveRootObject(asignaturas, toFile: Asignatura.ArchiveURL.path)
            
        }
        if let sourceViewController = sender.source as? PeriodoNoLectivoViewController {
            
            let valorInicio = sourceViewController.form.rowBy(tag: "inicio") as? DateRow
            let valorFinal = sourceViewController.form.rowBy(tag: "fin") as? DateRow
            var auxiliar = [PeriodoLectivo]()
            
            if let inicio = valorInicio?.value, let final = valorFinal?.value {
                let dato = PeriodoLectivo(fechaInicio: inicio, fechaFin: final)
                auxiliar.append(dato)
            }
            
            pLectivos = auxiliar
            
            NSKeyedArchiver.archiveRootObject(pLectivos, toFile: PeriodoLectivo.ArchiveURL.path)
            
        }
        if let sourceViewController = sender.source as? DiasNoLectivosViewController {
            
            let valores = sourceViewController.form.allRows
            var auxiliar = [DiasNoLectivos]()
            
            for elemento in valores {
                
                if let celda = elemento as? SplitRow<TextRow, DateRow> {
                    
                    if let nombre = celda.rowLeft?.value, let dia = celda.rowRight?.value {
                        
                        let nueva = DiasNoLectivos(nombre: nombre, dianol: dia)
                        auxiliar.append(nueva)
                    }
                    
                }
                
            }
            
            dNoLectivos = auxiliar
            
            NSKeyedArchiver.archiveRootObject(dNoLectivos, toFile: DiasNoLectivos.ArchiveURL.path)
            
        }
    }
    
}
