//
//  DiasNoLectivosViewController.swift
//  Proyecto
//
//  Created by Pierre Simon Tondreau on 15/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import Eureka

class DiasNoLectivosViewController: FormViewController {

    // MARK: Atributos
    var dias: [DiasNoLectivos] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Funciones de formulario
    /**
     Carga el formulario mediante el framework de Eureka
     */
    func loadForm(){
        form +++
            MultivaluedSection(multivaluedOptions: [.Reorder, .Insert, .Delete],
                               header: "Añadir los dias no lectivos") {
                                $0.addButtonProvider = { section in
                                    return ButtonRow(){
                                        $0.title = "Añadir Dia"
                                    }
                                }
                                for i in dias {
                                    let row = SplitRow<TextRow,DateRow>() { celda in
                                        celda.rowLeft = TextRow() {
                                            $0.value = i.nombre
                                        }
                                        celda.rowRight = DateRow() {
                                            $0.value = i.dianol
                                        }
                                        celda.rowLeftPercentage = CGFloat(0.5)
                                    }
                                    $0.append(row)
                                }
                                $0.multivaluedRowToInsertAt = { index in
                                    return SplitRow<TextRow,DateRow>(){
                                        $0.rowLeft = TextRow(){
                                            $0.placeholder = "Dia Festivo"
                                        }
                                        $0.rowRight = DateRow(){
                                            $0.value = Date()
                                        }
                                        $0.rowLeftPercentage = CGFloat(0.5)
                                    }
                                }
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        } else {
            fatalError("Not inside navigation controller.")
        }
    }
    
}
