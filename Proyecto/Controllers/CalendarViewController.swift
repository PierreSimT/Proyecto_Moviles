//
//  CalendarViewController.swift
//  Proyecto
//
//  Created by Macosx on 13/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarViewController: UIViewController {

    // MARK: Colores
    let outsideMonthColor = UIColor(colorWithHexValue: 0xC9CED7)
    let monthColor = UIColor(colorWithHexValue: 0x202740)
    let selectedMonthColor = UIColor(colorWithHexValue: 0xDAE9F8)
    //let currentDateSelectedViewColor
    
    // MARK: Atributos
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var tablaTareas: UITableView!
    @IBOutlet weak var fechaSeleccionada: UILabel!
    
    let diccionario: [DaysOfWeek : Int] = [ .monday: 1, .tuesday: 2, .wednesday: 3, .thursday: 4, .friday: 5]
    
    var selectedDate: CalendarCell?
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCalendarView()
        let currentDate = Date()
        calendarView.scrollToDate(currentDate, animateScroll: false)
        
        tablaTareas.dataSource = self
        tablaTareas.delegate = self
        
    }

    /**
     Configura el calendario
    */
    func setupCalendarView() {
        // Spacing
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        // Labels
        calendarView.visibleDates { (visibleDates) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
    }
    
    /**
     Colores del texto de las celdas
    */
    func handleCellTextColor (view: JTAppleCell?, cellState: CellState){
        guard let cell = view as? CalendarCell else {
            return
        }
        
        if cellState.isSelected {
            cell.dateLabel.textColor = monthColor
        } else {
            if (cellState.dateBelongsTo == .thisMonth) {
                cell.dateLabel.textColor = monthColor
            } else {
                cell.dateLabel.textColor = outsideMonthColor
            }
        }
    }
    
    /**
     Funcion llamada cada vez que se selecciona una celda del calendario
    */
    func handleSelectedCell(view: JTAppleCell?, cellState: CellState){
        guard let cell = view as? CalendarCell else { return }
        if cellState.isSelected {
            cell.selectedView.isHidden = false
            selectedDate = cell
            
            if ( cellState.dateBelongsTo != .thisMonth) {
                calendarView.scrollToDate(cellState.date, animateScroll: true)
            }
            formatter.dateFormat = "dd MMMM"
            fechaSeleccionada.text = "  "+formatter.string(from: cellState.date)
            tablaTareas.reloadData()
        } else {
            cell.selectedView.isHidden = true
            selectedDate = nil
            tablaTareas.reloadData()
        }
    }
    
    /**
    Inicializa los datos del texto que se encuentra en la parte superior del calendario
    */
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        
        formatter.dateFormat = "yyyy"
        year.text = formatter.string(from: date)
        
        formatter.dateFormat = "MMMM"
        month.text = formatter.string(from: date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let currentDate = Date()
        if UIDevice.current.orientation.isLandscape {
            calendarView.scrollToDate(currentDate, animateScroll: false)
        } else {
            calendarView.scrollToDate(currentDate, animateScroll: false)
        }
    }

}

// MARK: Calendario
extension CalendarViewController: JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.dateLabel.text = cellState.text
        cell.fecha = date
        cell.diaSemana = cellState.day
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "dd MM yyyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "01 01 2018")!
        let endDate = formatter.date(from: "31 12 2018")!
        let parameters: ConfigurationParameters
        
        if UIDevice.current.orientation.isLandscape {
            parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 1, firstDayOfWeek: .monday)
        }
        else {
            parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, firstDayOfWeek: .monday)
        }
        return parameters
    }
    
}

// MARK: Funciones de delegate de calendario
extension CalendarViewController: JTAppleCalendarViewDelegate {
    
    // Muestra la celda
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.dateLabel.text = cellState.text
        cell.fecha = date
        cell.diaSemana = cellState.day
        
        handleSelectedCell(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleSelectedCell(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleSelectedCell(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
}

// MARK: Table Functions
extension CalendarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedDate?.getDatos().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let fechaSel = selectedDate else {
            fatalError("Casilla aun no seleccionada")
        }
        
        let idCell = "TareaTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: idCell, for: indexPath) as? CalendarTableViewCell else {
            fatalError("La celda no es una instancia de CalendarTableViewCell")
        }
        
        let elementos = fechaSel.getDatos()
        let elemento = elementos[indexPath.row]
        
        if let asignatura = elemento as? Asignatura{
            let fecha = asignatura.horarioInicio[diccionario[(selectedDate?.diaSemana)!]!]
            let fechaFin = asignatura.horarioFin[diccionario[(selectedDate?.diaSemana)!]!]
            
            let calendar = Calendar.current
            let horaInicio = calendar.component(.hour, from: fecha!)
            let minutoInicio = calendar.component(.minute, from: fecha!)
            
            let horaFin = calendar.component(.hour, from: fechaFin!)
            let minutoFin = calendar.component(.minute, from: fechaFin!)
            
            let dsc: String = horaInicio.description + " : " + minutoInicio.description
            let fin: String = horaFin.description + " : " + minutoFin.description
            
            cell.nombre.text = asignatura.nombreAsignatura
            cell.inicio.text = dsc
            cell.fin.text = fin
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

// MARK: Funciones Auxiliares para colores de calendario
extension UIColor {
    convenience init(colorWithHexValue value: Int, alpha: CGFloat = 1.0){
        self.init (red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
                   green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
                   blue: CGFloat(value & 0x0000FF) / 255.0,
                   alpha: alpha)
    }
}

