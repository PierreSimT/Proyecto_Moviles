//
//  HomeViewController.swift
//  Proyecto
//
//  Created by Macosx on 6/4/18.
//  Copyright © 2018 Grupo. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Propiedades
    var tareas: [Tarea] = []
    var tareasPorSeccion: [String: [Tarea]] = [:]
    var clavesSeccion: [Int: String] = [:]
    
    @IBOutlet weak var tablaTareas: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cargaDatos()
        cargaPorFecha()
        tablaTareas.dataSource = self
        tablaTareas.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: TableView functions
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tareasPorSeccion.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tareasSeccion = tareasPorSeccion[clavesSeccion[section]!]
        return (tareasSeccion?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idCell = "TareaTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: idCell, for: indexPath) as? TareaTableViewCell else {
            fatalError("La celda no es una instancia de TareaTableViewCell")
        }
        
        // Obtenemos la seccion
        let seccion = indexPath.section
        let vector = tareasPorSeccion[clavesSeccion[seccion]!]
        
        // Obtenemos los atributos
        let tarea = vector![indexPath.row]
        let fecha = tarea.Dia
        let tiempoRestante = calcularTiempo(inicio: Date(), final: fecha)
        let calendar = Calendar.current
        let hora = calendar.component(.hour, from: fecha)
        let minuto = calendar.component(.minute, from: fecha)
        let dsc: String = hora.description + " : " + minuto.description
        
        
        // Asignatmos los datos a la celda
        cell.nombre.text = tarea.Descripcion
        cell.horario.text = dsc
        cell.descripcion.text = tiempoRestante
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // Tamaño del header de la seccion
        return CGFloat(25)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return clavesSeccion[section]
    }
    
    /**
        Funcion que realiza el unwind de la vista ConfiguracionTableViewController
    */
    @IBAction func unwindToHome(sender: UIStoryboardSegue) {
        cargaDatos()
        cargaPorFecha()
        tablaTareas.reloadData()
    }
    
    // MARK: Carga de datos
    /**
        Funcion que carga las tareas guardadas mediante la persistencia de datos
    */
    private func cargaDatos() {
        tareas.removeAll()
        if let datos = NSKeyedUnarchiver.unarchiveObject(withFile: Tarea.ArchiveURL.path) as? [Tarea] {
            let hoy = Date()
            for tarea in datos {
                if hoy < tarea.Dia {
                    tareas.append(tarea)
                }
            }
            
        }
        
    }
    
    /**
        Funcion que ordena las tareas cargadas por fechas, para poder crear las secciones correspondientes
    */
    private func cargaPorFecha () {
        
        tareasPorSeccion.removeAll()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        var indice = -1
        var anterior: String = ""
        tareas.sort { (tarea1, tarea2) -> Bool in
            return tarea1.Dia.compare(tarea2.Dia) == ComparisonResult.orderedAscending
        }
        for tarea in tareas {
            
            let fecha = formatter.string(from: tarea.Dia)
            if fecha != anterior {
                indice += 1
                anterior = fecha
                clavesSeccion[indice] = fecha
                tareasPorSeccion[fecha] = []
            }
            tareasPorSeccion[fecha]?.append(tarea)
        }
    }
    
    
    // MARK: Funciones auxiliares
    /**
        Funcion que calcula el tiempo aproximado hasta que se alcanze la realizacion de la tarea.
     - parameter inicio: Fecha inicial
     - parameter final: Fecha final
     - returns: string con la descripcion del tiempo restante
    */
    private func calcularTiempo (inicio: Date, final: Date) -> String {
        
        let intervaloSegundos = final.timeIntervalSince(inicio)
        let intervaloMinutos = intervaloSegundos / 60
        let intervaloHoras = intervaloMinutos / 60
        let intervaloDias = intervaloHoras / 24
        let intervaloMeses = intervaloDias / 31
        let intervaloAnios = intervaloMeses / 12
        
        if intervaloAnios >= 1 {
            let descripcion = "Quedan " + Int(intervaloAnios).description + " años"
            return descripcion
        }
            
        if intervaloMeses >= 1 {
            let descripcion = "Quedan " + Int(intervaloMeses).description + " meses"
            return descripcion
        }
            
        if intervaloDias >= 1 {
            let descripcion = "Quedan " + Int(intervaloDias).description + " dias"
            return descripcion
        }
            
        if intervaloHoras >= 1 {
            let descripcion = "Quedan " + Int(intervaloHoras).description + " horas"
            return descripcion
        }
        
        if intervaloMinutos >= 1 {
            let descripcion = "Quedan " + Int(intervaloMinutos).description + " minutos"
            return descripcion
        }

        let descripcion = "Quedan " + Int(intervaloSegundos).description + " segundos"
        return descripcion
        
    }
    
}
